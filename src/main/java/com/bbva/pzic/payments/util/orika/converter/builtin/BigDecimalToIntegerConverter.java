package com.bbva.pzic.payments.util.orika.converter.builtin;

import com.bbva.pzic.payments.util.orika.converter.BidirectionalConverter;
import com.bbva.pzic.payments.util.orika.metadata.Type;

import java.math.BigDecimal;

public class BigDecimalToIntegerConverter extends BidirectionalConverter<BigDecimal, Integer> {

    @Override
    public Integer convertTo(BigDecimal source, Type<Integer> destinationType) {
        return source == null ? null : source.intValue();
    }

    @Override
    public BigDecimal convertFrom(Integer source, Type<BigDecimal> destinationType) {
        return source == null ? null : new BigDecimal(source);
    }
}
