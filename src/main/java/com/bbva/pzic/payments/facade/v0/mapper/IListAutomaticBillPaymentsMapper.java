package com.bbva.pzic.payments.facade.v0.mapper;

import com.bbva.pzic.payments.business.dto.DTOIntAutomaticBillPayments;
import com.bbva.pzic.payments.business.dto.InputListAutomaticBillPayments;
import com.bbva.pzic.payments.canonic.AutomaticBillPayments;

/**
 * Created on 15/08/2018.
 *
 * @author Entelgy
 */
public interface IListAutomaticBillPaymentsMapper {

    InputListAutomaticBillPayments mapIn(String paginationKey, Integer pageSize, String statusId, String serviceServiceTypeId, String serviceId,
                                         String fromStartDate, String toStartDate);

    AutomaticBillPayments mapOut(DTOIntAutomaticBillPayments dtoIntAutomaticBillPayments);
}