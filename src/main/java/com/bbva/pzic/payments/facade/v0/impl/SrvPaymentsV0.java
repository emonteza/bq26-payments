package com.bbva.pzic.payments.facade.v0.impl;

import com.bbva.jee.arq.spring.core.servicing.annotations.SMC;
import com.bbva.jee.arq.spring.core.servicing.annotations.SN;
import com.bbva.jee.arq.spring.core.servicing.annotations.VN;
import com.bbva.jee.arq.spring.core.servicing.utils.BusinessServicesToolKit;
import com.bbva.pzic.payments.business.ISrvIntPayments;
import com.bbva.pzic.payments.business.dto.DTOIntAutomaticBillPayments;
import com.bbva.pzic.payments.canonic.AutomaticBillPayment;
import com.bbva.pzic.payments.canonic.AutomaticBillPaymentData;
import com.bbva.pzic.payments.canonic.AutomaticBillPayments;
import com.bbva.pzic.payments.facade.v0.ISrvPaymentsV0;
import com.bbva.pzic.payments.facade.v0.mapper.ICreateAutomaticBillPaymentsMapper;
import com.bbva.pzic.payments.facade.v0.mapper.IListAutomaticBillPaymentsMapper;
import com.bbva.pzic.payments.util.mappers.PaginationMapper;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.ws.rs.*;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;

/**
 * Created on 15/08/2018.
 *
 * @author Entelgy
 */
@Path("/v0")
@Produces(MediaType.APPLICATION_JSON)
@SN(registryID = "SNPE1700008", logicalID = "payments")
@VN(vnn = "v0")
@Service
public class SrvPaymentsV0
        implements
        ISrvPaymentsV0,
        com.bbva.jee.arq.spring.core.servicing.utils.ContextAware {

    private static final Log LOG = LogFactory.getLog(SrvPaymentsV0.class);
    public HttpHeaders httpHeaders;
    public UriInfo uriInfo;
    @Autowired
    private BusinessServicesToolKit businessServicesToolKit;
    @Autowired
    private ISrvIntPayments srvIntPayments;
    @Autowired
    private IListAutomaticBillPaymentsMapper listAutomaticBillPaymentsMapper;
    @Autowired
    private ICreateAutomaticBillPaymentsMapper createAutomaticBillPaymentsMapper;

    /**
     * {@inheritDoc}
     */
    @Override
    @GET
    @Path("/automatic-bill-payments")
    @SMC(registryID = "SMCPE1710013", logicalID = "listAutomaticBillPayments")
    public AutomaticBillPayments listAutomaticBillPayments(
            @QueryParam("paginationKey") final String paginationKey,
            @QueryParam("pageSize") final Integer pageSize,
            @QueryParam("status.id") final String statusId,
            @QueryParam("service.serviceType.id") final String serviceServiceTypeId,
            @QueryParam("service.id") final String serviceId,
            @QueryParam("fromStartDate") final String fromStartDate,
            @QueryParam("toStartDate") final String toStartDate) {
        LOG.info("----- Invoking service listAutomaticBillPayments -----");

        DTOIntAutomaticBillPayments dTOIntAutomaticBillPayments = srvIntPayments
                .listAutomaticBillPayments(listAutomaticBillPaymentsMapper
                        .mapIn(paginationKey, pageSize, statusId,
                                serviceServiceTypeId, serviceId, fromStartDate,
                                toStartDate));

        AutomaticBillPayments automaticBillPayments = listAutomaticBillPaymentsMapper
                .mapOut(dTOIntAutomaticBillPayments);
        if (automaticBillPayments == null) {
            return null;
        }
        if (dTOIntAutomaticBillPayments.getPagination() == null) {
            return automaticBillPayments;
        }
        automaticBillPayments.setPagination(PaginationMapper
                .build(businessServicesToolKit
                        .getPaginationBuider()
                        .setPagination(
                                SrvPaymentsV0.class,
                                "listAutomaticBillPayments",
                                uriInfo,
                                dTOIntAutomaticBillPayments.getPagination()
                                        .getPaginationKey(),
                                null,
                                dTOIntAutomaticBillPayments.getPagination()
                                        .getPageSize(), null, null, null, null)
                        .build()));
        return automaticBillPayments;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @POST
    @Path("/automatic-bill-payments")
    @Consumes(MediaType.APPLICATION_JSON)
    @SMC(registryID = "SMCPE1710014", logicalID = "createAutomaticBillPayments")
    public AutomaticBillPaymentData createAutomaticBillPayments(
            final AutomaticBillPayment automaticBillPayment) {
        LOG.info("----- Invoking service createAutomaticBillPayments -----");
        return createAutomaticBillPaymentsMapper.mapOut(srvIntPayments
                .createAutomaticBillPayments(createAutomaticBillPaymentsMapper
                        .mapIn(automaticBillPayment)));
    }

    @Override
    public void setHttpHeaders(HttpHeaders httpHeaders) {
        this.httpHeaders = httpHeaders;
    }

    @Override
    public void setUriInfo(UriInfo uriInfo) {
        this.uriInfo = uriInfo;
    }
}