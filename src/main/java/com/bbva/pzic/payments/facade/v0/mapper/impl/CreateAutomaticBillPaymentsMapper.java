package com.bbva.pzic.payments.facade.v0.mapper.impl;

import com.bbva.pzic.payments.business.dto.DTOIntAutomaticBillPayment;
import com.bbva.pzic.payments.canonic.AutomaticBillPayment;
import com.bbva.pzic.payments.canonic.AutomaticBillPaymentData;
import com.bbva.pzic.payments.facade.v0.mapper.ICreateAutomaticBillPaymentsMapper;
import com.bbva.pzic.payments.util.mappers.Mapper;
import com.bbva.pzic.payments.util.orika.MapperFactory;
import com.bbva.pzic.payments.util.orika.impl.ConfigurableMapper;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Created on 15/08/2018.
 *
 * @author Entelgy
 */
@Mapper
public class CreateAutomaticBillPaymentsMapper extends ConfigurableMapper
        implements
        ICreateAutomaticBillPaymentsMapper {

    private static final Log LOG = LogFactory
            .getLog(CreateAutomaticBillPaymentsMapper.class);

    /**
     * {@inheritDoc}
     */
    @Override
    public DTOIntAutomaticBillPayment mapIn(
            final AutomaticBillPayment automaticBillPayment) {
        LOG.info("... called method CreateAutomaticBillPaymentsMapper.mapIn ...");
        DTOIntAutomaticBillPayment dtoInt = map(automaticBillPayment,
                DTOIntAutomaticBillPayment.class);
        return dtoInt;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public AutomaticBillPaymentData mapOut(
            final AutomaticBillPayment automaticBillPayment) {
        LOG.info("... called method CreateAutomaticBillPaymentsMapper.mapOut ...");
        if (automaticBillPayment == null) {
            return null;
        }
        AutomaticBillPaymentData automaticBillPaymentData = new AutomaticBillPaymentData();
        automaticBillPaymentData.setData(automaticBillPayment);
        return automaticBillPaymentData;
    }

    @Override
    protected void configure(MapperFactory factory) {
        super.configure(factory);
        /*factory.classMap(AutomaticBillPayment.class,
                DTOIntAutomaticBillPayment.class)
                .field("service.id",
                        "concatenar(DTOautomaticBillPayment.service.id, DTOautomaticBillPayment.service.serviceType.id)")
                .field("bill.id", "DTOautomaticBillPayment.bill.id")
                .field("startDate", "DTOautomaticBillPayment.startDate")
                .field("origin.contractId",
                        "DTOautomaticBillPayment.origin.contractId")
                .field("amountMax.value",
                        "DTOautomaticBillPayment.amountMax.value")
                .field("amountMax.currency",
                        "DTOautomaticBillPayment.amountMax.currency")
                .field("additionalFields[0].value",
                        "DTOautomaticBillPayment.additionalFields[0].additionalField.value")
                .register();*/
    }
}