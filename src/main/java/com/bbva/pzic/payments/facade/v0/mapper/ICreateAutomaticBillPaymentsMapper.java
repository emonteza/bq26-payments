package com.bbva.pzic.payments.facade.v0.mapper;

import com.bbva.pzic.payments.business.dto.DTOIntAutomaticBillPayment;
import com.bbva.pzic.payments.canonic.AutomaticBillPayment;
import com.bbva.pzic.payments.canonic.AutomaticBillPaymentData;

/**
 * Created on 15/08/2018.
 *
 * @author Entelgy
 */
public interface ICreateAutomaticBillPaymentsMapper {

    DTOIntAutomaticBillPayment mapIn(AutomaticBillPayment automaticBillPayment);

    AutomaticBillPaymentData mapOut(AutomaticBillPayment automaticBillPayment);
}