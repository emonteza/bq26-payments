package com.bbva.pzic.payments.canonic;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

/**
 * @author Entelgy
 */
@XmlRootElement(name = "service", namespace = "urn:com:bbva:pzic:payments:canonic")
@XmlType(name = "service", namespace = "urn:com:bbva:pzic:payments:canonic")
@XmlAccessorType(XmlAccessType.FIELD)
public class Service implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Service identifier.
     */
    private String id;
    /**
     * Service type.
     */
    private ServiceType serviceType;
    /**
     * Service name.
     */
    private String name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public ServiceType getServiceType() {
        return serviceType;
    }

    public void setServiceType(ServiceType serviceType) {
        this.serviceType = serviceType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}