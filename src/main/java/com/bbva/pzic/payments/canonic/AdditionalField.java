package com.bbva.pzic.payments.canonic;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

/**
 * @author Entelgy
 */
@XmlRootElement(name = "additionalField", namespace = "urn:com:bbva:pzic:payments:canonic")
@XmlType(name = "additionalField", namespace = "urn:com:bbva:pzic:payments:canonic")
@XmlAccessorType(XmlAccessType.FIELD)
public class AdditionalField implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Field value.
     */
    private String value;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}