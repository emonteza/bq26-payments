package com.bbva.pzic.payments.canonic;

import com.bbva.jee.arq.spring.core.servicing.utils.ShortDateAdapter;

import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @author Entelgy
 */
@XmlRootElement(name = "automaticBillPayment", namespace = "urn:com:bbva:pzic:payments:canonic")
@XmlType(name = "automaticBillPayment", namespace = "urn:com:bbva:pzic:payments:canonic")
@XmlAccessorType(XmlAccessType.FIELD)
public class AutomaticBillPayment implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Referred service in the payment process.
     */
    private Service service;
    /**
     * Information about the bill.
     */
    private Bill bill;
    /**
     * String based on ISO-8601 date format for providing the date when the
     * payment is planned to be performed. If the customer does not enter a date
     * then it will be set the creation date of the automatic bill payment
     * (YYYY-MM-DD).
     */
    @XmlSchemaType(name = "date")
    @XmlJavaTypeAdapter(ShortDateAdapter.class)
    private Date startDate;
    /**
     * Customer\'s contract that is used in payment process.
     */
    private Origin origin;
    /**
     * Maximum amount that is used in payment process.
     */
    private AmountMax amountMax;
    /**
     * List of values for the additional form fields of the service paid. For
     * each additional form field, a key-value pair will be provided.
     */
    private List<AdditionalField> additionalFields;
    /**
     * Automatic bill payment identifier.
     */
    private String id;
    /**
     * Status of the payment.
     */
    private Status status;
    /**
     * String based on ISO-8601 date format for providing the date when the
     * payment has been activated (YYYY-MM-DD).
     */
    @XmlSchemaType(name = "date")
    @XmlJavaTypeAdapter(ShortDateAdapter.class)
    private Date activatedDate;
    /**
     * String based on ISO-8601 date format for providing the date when the
     * payment has been deactivated (YYYY-MM-DD).
     */
    @XmlSchemaType(name = "date")
    @XmlJavaTypeAdapter(ShortDateAdapter.class)
    private Date deactivatedDate;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    public Bill getBill() {
        return bill;
    }

    public void setBill(Bill bill) {
        this.bill = bill;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Origin getOrigin() {
        return origin;
    }

    public void setOrigin(Origin origin) {
        this.origin = origin;
    }

    public AmountMax getAmountMax() {
        return amountMax;
    }

    public void setAmountMax(AmountMax amountMax) {
        this.amountMax = amountMax;
    }

    public List<AdditionalField> getAdditionalFields() {
        return additionalFields;
    }

    public void setAdditionalFields(List<AdditionalField> additionalFields) {
        this.additionalFields = additionalFields;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Date getActivatedDate() {
        return activatedDate;
    }

    public void setActivatedDate(Date activatedDate) {
        this.activatedDate = activatedDate;
    }

    public Date getDeactivatedDate() {
        return deactivatedDate;
    }

    public void setDeactivatedDate(Date deactivatedDate) {
        this.deactivatedDate = deactivatedDate;
    }
}