package com.bbva.pzic.payments.canonic;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

/**
 * @author Entelgy
 */
@XmlRootElement(name = "amountMax", namespace = "urn:com:bbva:pzic:payments:canonic")
@XmlType(name = "amountMax", namespace = "urn:com:bbva:pzic:payments:canonic")
@XmlAccessorType(XmlAccessType.FIELD)
public class AmountMax implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Value of maximum amount that is used in payment process.
     */
    private Integer value;
    /**
     * String based on ISO-4217 for specifying the currency of maximum amount
     * that is used in payment process.
     */
    private String currency;

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }
}