package com.bbva.pzic.payments.business.dto;

import com.bbva.pzic.payments.canonic.AutomaticBillPayment;

import java.util.List;

/**
 * Created on 15/08/2018.
 *
 * @author Entelgy
 */
public class DTOIntAutomaticBillPayments {

    private List<AutomaticBillPayment> data;
    private DTOIntPagination pagination;

    public List<AutomaticBillPayment> getData() {
        return data;
    }

    public void setData(List<AutomaticBillPayment> data) {
        this.data = data;
    }

    public DTOIntPagination getPagination() {
        return pagination;
    }

    public void setPagination(DTOIntPagination pagination) {
        this.pagination = pagination;
    }
}