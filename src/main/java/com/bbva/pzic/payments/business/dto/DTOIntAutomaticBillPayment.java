package com.bbva.pzic.payments.business.dto;

import javax.validation.Valid;
import java.util.Date;
import java.util.List;

/**
 * Created on 15/08/2018.
 *
 * @author Entelgy
 */
public class DTOIntAutomaticBillPayment {

    @Valid
    private DTOIntService service;
    @Valid
    private DTOIntBill bill;
    private Date startDate;
    @Valid
    private DTOIntOrigin origin;
    @Valid
    private DTOIntAmountMax amountMax;
    @Valid
    private List<DTOIntAdditionalField> additionalFields;
    private String id;
    private DTOIntStatus status;
    private Date activatedDate;
    private Date deactivatedDate;

    public DTOIntService getService() {
        return service;
    }

    public void setService(DTOIntService service) {
        this.service = service;
    }

    public DTOIntBill getBill() {
        return bill;
    }

    public void setBill(DTOIntBill bill) {
        this.bill = bill;
    }

    public Date getStartDate() {
        if (startDate == null) {
            return null;
        }
        return new Date(startDate.getTime());
    }

    public void setStartDate(Date startDate) {
        if (startDate == null) {
            this.startDate = null;
        } else {
            this.startDate = new Date(startDate.getTime());
        }
    }

    public DTOIntOrigin getOrigin() {
        return origin;
    }

    public void setOrigin(DTOIntOrigin origin) {
        this.origin = origin;
    }

    public DTOIntAmountMax getAmountMax() {
        return amountMax;
    }

    public void setAmountMax(DTOIntAmountMax amountMax) {
        this.amountMax = amountMax;
    }

    public List<DTOIntAdditionalField> getAdditionalFields() {
        return additionalFields;
    }

    public void setAdditionalFields(List<DTOIntAdditionalField> additionalFields) {
        this.additionalFields = additionalFields;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public DTOIntStatus getStatus() {
        return status;
    }

    public void setStatus(DTOIntStatus status) {
        this.status = status;
    }

    public Date getActivatedDate() {
        if (activatedDate == null) {
            return null;
        }
        return new Date(activatedDate.getTime());
    }

    public void setActivatedDate(Date activatedDate) {
        if (activatedDate == null) {
            this.activatedDate = null;
        } else {
            this.activatedDate = new Date(activatedDate.getTime());
        }
    }

    public Date getDeactivatedDate() {
        if (deactivatedDate == null) {
            return null;
        }
        return new Date(deactivatedDate.getTime());
    }

    public void setDeactivatedDate(Date deactivatedDate) {
        if (deactivatedDate == null) {
            this.deactivatedDate = null;
        } else {
            this.deactivatedDate = new Date(deactivatedDate.getTime());
        }
    }
}