package com.bbva.pzic.payments.business.dto;

import javax.validation.constraints.NotNull;

/**
 * Created on 15/08/2018.
 *
 * @author Entelgy
 */
public class DTOIntAdditionalField {

    @NotNull(groups = ValidationGroup.CreateAutomaticBillPayments.class)
    private String value;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}