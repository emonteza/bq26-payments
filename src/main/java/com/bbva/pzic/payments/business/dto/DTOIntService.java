package com.bbva.pzic.payments.business.dto;

import javax.validation.constraints.NotNull;

/**
 * Created on 15/08/2018.
 *
 * @author Entelgy
 */
public class DTOIntService {

    @NotNull(groups = ValidationGroup.CreateAutomaticBillPayments.class)
    private String id;
    private DTOIntServiceType serviceType;
    private String name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public DTOIntServiceType getServiceType() {
        return serviceType;
    }

    public void setServiceType(DTOIntServiceType serviceType) {
        this.serviceType = serviceType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}