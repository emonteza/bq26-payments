package com.bbva.pzic.payments.business.dto;

import javax.validation.constraints.Digits;
import javax.validation.constraints.Size;

/**
 * Created on 15/08/2018.
 *
 * @author Entelgy
 */
public class InputListAutomaticBillPayments {

    @Size(max = 20, groups = {ValidationGroup.ListAutomaticBillPayments.class})
    private String paginationKey;
    @Digits(integer = 3, fraction = 0, groups = {ValidationGroup.ListAutomaticBillPayments.class})
    private Integer pageSize;
    @Size(max = 1, groups = {ValidationGroup.ListAutomaticBillPayments.class})
    private String statusId;
    @Size(max = 2, groups = {ValidationGroup.ListAutomaticBillPayments.class})
    private String serviceServiceTypeId;
    @Size(max = 7, groups = {ValidationGroup.ListAutomaticBillPayments.class})
    private String serviceId;
    @Size(max = 10, groups = {ValidationGroup.ListAutomaticBillPayments.class})
    private String fromStartDate;
    @Size(max = 10, groups = {ValidationGroup.ListAutomaticBillPayments.class})
    private String toStartDate;

    public String getPaginationKey() {
        return paginationKey;
    }

    public void setPaginationKey(String paginationKey) {
        this.paginationKey = paginationKey;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public String getStatusId() {
        return statusId;
    }

    public void setStatusId(String statusId) {
        this.statusId = statusId;
    }

    public String getServiceServiceTypeId() {
        return serviceServiceTypeId;
    }

    public void setServiceServiceTypeId(String serviceServiceTypeId) {
        this.serviceServiceTypeId = serviceServiceTypeId;
    }

    public String getServiceId() {
        return serviceId;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

    public String getFromStartDate() {
        return fromStartDate;
    }

    public void setFromStartDate(String fromStartDate) {
        this.fromStartDate = fromStartDate;
    }

    public String getToStartDate() {
        return toStartDate;
    }

    public void setToStartDate(String toStartDate) {
        this.toStartDate = toStartDate;
    }
}