package com.bbva.pzic.payments.business.dto;

import javax.validation.constraints.NotNull;

/**
 * Created on 15/08/2018.
 *
 * @author Entelgy
 */
public class DTOIntAmountMax {

    @NotNull(groups = ValidationGroup.CreateAutomaticBillPayments.class)
    private Integer value;
    @NotNull(groups = ValidationGroup.CreateAutomaticBillPayments.class)
    private String currency;

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }
}