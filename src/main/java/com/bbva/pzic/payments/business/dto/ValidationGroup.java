package com.bbva.pzic.payments.business.dto;

/**
 * Created on 15/08/2018.
 *
 * @author Entelgy
 */
public interface ValidationGroup {

    interface CreateAutomaticBillPayments {
    }

    interface ListAutomaticBillPayments {
    }
}