package com.bbva.pzic.payments.business.dto;

import javax.validation.constraints.NotNull;

/**
 * Created on 15/08/2018.
 *
 * @author Entelgy
 */
public class DTOIntBill {

    @NotNull(groups = ValidationGroup.CreateAutomaticBillPayments.class)
    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}