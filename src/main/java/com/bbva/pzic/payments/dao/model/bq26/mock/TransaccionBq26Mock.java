package com.bbva.pzic.payments.dao.model.bq26.mock;

import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.jee.arq.spring.core.host.protocolo.ps9.aplicacion.CopySalida;
import com.bbva.jee.arq.spring.core.servicing.gce.BusinessServiceException;
import com.bbva.pzic.payments.dao.model.bq26.FormatoBGMQ26E;
import com.bbva.pzic.payments.dao.model.bq26.FormatoBGMQ26S;
import com.bbva.pzic.payments.dao.model.bq26.PeticionTransaccionBq26;
import com.bbva.pzic.payments.dao.model.bq26.RespuestaTransaccionBq26;
import com.bbva.pzic.payments.util.Errors;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Invocador de la transacci&oacute;n <code>BQ26</code>
 *
 * @see com.bbva.pzic.payments.dao.model.bq26.PeticionTransaccionBq26
 * @see com.bbva.pzic.payments.dao.model.bq26.RespuestaTransaccionBq26
 */
@Component("transaccionBq26")
public class TransaccionBq26Mock implements InvocadorTransaccion<PeticionTransaccionBq26, RespuestaTransaccionBq26> {

    public static final String TEST_EMPTY = "9999";
    public static final String TEST_NOT_PAGINATION = "8888";

    private FormatsBq26Mock mock;

    @PostConstruct
    private void init() {
        mock = new FormatsBq26Mock();
    }

    @Override
    public RespuestaTransaccionBq26 invocar(PeticionTransaccionBq26 transaccion) {
        final RespuestaTransaccionBq26 response = new RespuestaTransaccionBq26();
        response.setCodigoControl("OK");
        response.setCodigoRetorno("OK_COMMIT");

        final FormatoBGMQ26E format = transaccion.getCuerpo().getParte(FormatoBGMQ26E.class);

        String serviceId = format.getConveni();

        if (TEST_EMPTY.equalsIgnoreCase(serviceId)) {
            return response;
        }
        try {
            if (TEST_NOT_PAGINATION.equalsIgnoreCase(serviceId)) {
                response.getCuerpo().getPartes().addAll(buildDataCopies());

            } else {
                response.getCuerpo().getPartes().addAll(buildDataCopies());
                response.getCuerpo().getPartes().add(buildPaginationCopy());
            }
        } catch (IOException e) {
            throw new BusinessServiceException(Errors.TECHNICAL_ERROR, e);
        }
        return response;
    }

    @Override
    public RespuestaTransaccionBq26 invocarCache(PeticionTransaccionBq26 transaccion) {
        return null;
    }

    @Override
    public void vaciarCache() {
    }

    private List<CopySalida> buildDataCopies() throws IOException {
        List<FormatoBGMQ26S> formats = mock.getFormatsBGMQ26S();

        List<CopySalida> copies = new ArrayList<>();
        for (FormatoBGMQ26S format : formats) {
            CopySalida copy = new CopySalida();
            copy.setCopy(format);
            copies.add(copy);
        }
        return copies;
    }

    private CopySalida buildPaginationCopy() throws IOException {
        CopySalida copy = new CopySalida();
        copy.setCopy(mock.getFormatBGMQ26P());
        return copy;
    }
}