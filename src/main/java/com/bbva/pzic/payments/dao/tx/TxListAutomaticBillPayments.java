package com.bbva.pzic.payments.dao.tx;

import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.pzic.payments.business.dto.DTOIntAutomaticBillPayments;

import com.bbva.pzic.payments.business.dto.InputListAutomaticBillPayments;
import com.bbva.pzic.payments.dao.model.bq26.*;
import com.bbva.pzic.payments.dao.tx.mapper.ITxListAutomaticBillPaymentsMapper;
import com.bbva.pzic.payments.util.tx.AbstractDoubleTransaction;
import com.bbva.pzic.payments.util.tx.Tx;

import javax.annotation.Resource;

/**
 * Created on 15/08/2018.
 *
 * @author Entelgy
 */
@Tx("txListAutomaticBillPayments")
public class TxListAutomaticBillPayments extends AbstractDoubleTransaction<InputListAutomaticBillPayments, FormatoBGMQ26E, DTOIntAutomaticBillPayments, FormatoBGMQ26S, FormatoBGMQ26P> {

    @Resource(name = "txListAutomaticBillPaymentsMapper")
    private ITxListAutomaticBillPaymentsMapper mapper;

    @Resource(name = "transaccionBq26")
    private InvocadorTransaccion<PeticionTransaccionBq26, RespuestaTransaccionBq26> transaccionBq26;


    @Override
    protected FormatoBGMQ26E mapDtoInToRequestFormat(InputListAutomaticBillPayments dtoIn) {
        return mapper.mapIn(dtoIn);
    }

    @Override
    protected DTOIntAutomaticBillPayments mapResponseFormatToDtoOut(FormatoBGMQ26S formatOutput, InputListAutomaticBillPayments dtoIn, DTOIntAutomaticBillPayments dtoOut) {
        return mapper.mapOut(formatOutput,dtoOut);
    }

    @Override
    protected DTOIntAutomaticBillPayments mapResponseFormatToDtoOut2(FormatoBGMQ26P formatOutput, InputListAutomaticBillPayments dtoIn, DTOIntAutomaticBillPayments dtoOut) {
        return mapper.mapOut2(formatOutput,dtoOut);
    }


    @Override
    protected InvocadorTransaccion<?, ?> getTransaction() {
        return transaccionBq26;
    }
}