package com.bbva.pzic.payments.dao;

import com.bbva.pzic.payments.business.dto.DTOIntAutomaticBillPayment;
import com.bbva.pzic.payments.business.dto.DTOIntAutomaticBillPayments;
import com.bbva.pzic.payments.business.dto.InputListAutomaticBillPayments;
import com.bbva.pzic.payments.canonic.AutomaticBillPayment;

/**
 * Created on 15/08/2018.
 *
 * @author Entelgy
 */
public interface IPaymentsDAO {

    DTOIntAutomaticBillPayments listAutomaticBillPayments(InputListAutomaticBillPayments input);

    AutomaticBillPayment createAutomaticBillPayments(DTOIntAutomaticBillPayment dtoInt);
}