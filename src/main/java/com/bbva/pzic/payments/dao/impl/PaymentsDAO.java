package com.bbva.pzic.payments.dao.impl;

import com.bbva.pzic.payments.business.dto.DTOIntAutomaticBillPayment;
import com.bbva.pzic.payments.business.dto.DTOIntAutomaticBillPayments;
import com.bbva.pzic.payments.business.dto.InputListAutomaticBillPayments;
import com.bbva.pzic.payments.canonic.AutomaticBillPayment;
import com.bbva.pzic.payments.dao.IPaymentsDAO;
import com.bbva.pzic.payments.dao.tx.TxCreateAutomaticBillPayments;
import com.bbva.pzic.payments.dao.tx.TxListAutomaticBillPayments;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * Created on 15/08/2018.
 *
 * @author Entelgy
 */
@Repository
public class PaymentsDAO implements IPaymentsDAO {

    private static final Log LOG = LogFactory.getLog(PaymentsDAO.class);

    @Autowired
    private TxListAutomaticBillPayments txListAutomaticBillPayments;
    @Autowired
    private TxCreateAutomaticBillPayments txCreateAutomaticBillPayments;

    /**
     * {@inheritDoc}
     */
    @Override
    public DTOIntAutomaticBillPayments listAutomaticBillPayments(
            final InputListAutomaticBillPayments input) {
        LOG.info("... Invoking method PaymentsDAO.listAutomaticBillPayments ...");
        return txListAutomaticBillPayments.invoke(input);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public AutomaticBillPayment createAutomaticBillPayments(
            final DTOIntAutomaticBillPayment dtoInt) {
        LOG.info("... Invoking method PaymentsDAO.createAutomaticBillPayments ...");
//        return txCreateAutomaticBillPayments.invoke(dtoInt);
        return null;
    }
}