package com.bbva.pzic.payments.dao.tx;

import com.bbva.pzic.payments.dao.tx.mapper.ITxCreateAutomaticBillPaymentsMapper;
import com.bbva.pzic.payments.util.tx.Tx;

import javax.annotation.Resource;

/**
 * Created on 15/08/2018.
 *
 * @author Entelgy
 */
@Tx("txCreateAutomaticBillPayments")
public class TxCreateAutomaticBillPayments {

    @Resource(name = "txCreateAutomaticBillPaymentsMapper")
    private ITxCreateAutomaticBillPaymentsMapper mapper;
}