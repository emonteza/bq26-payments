package com.bbva.pzic.payments.dao.model.bq26;

import com.bbva.jee.arq.spring.core.host.Atributo;
import com.bbva.jee.arq.spring.core.host.Cuerpo;
import com.bbva.jee.arq.spring.core.host.CuerpoMultiparte;
import com.bbva.jee.arq.spring.core.host.MensajeMultiparte;
import com.bbva.jee.arq.spring.core.host.Multiformato;
import com.bbva.jee.arq.spring.core.host.Transaccion;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

/**
 * <p>Transacci&oacute;n <code>BQ26</code></p>
 * <p>Informaci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Tipo:</b> 1</li>
 *    <li><b>Subtipo:</b> 1</li>
 *    <li><b>Versi&oacute;n:</b> 1</li>
 *    <li><b>Clase de petici&oacute;n:</b> PeticionTransaccionBq26</li>
 *    <li><b>Clase de respuesta:</b> RespuestaTransaccionBq26</li>
 * </ul>
 * </p>
 * <p>Configuraci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Nombre configuraci&oacute;n:</b> default_ps9_mx</li>
 * </ul>
 * </p>
 * <p>Copy de la transacci&oacute;n:</p>
 * <code><pre> * FICHERO: PEBT.QGFD.FIX.QGDTCCT.BQ26.D1170920
 * BQ26PAGO FACIL-LISTA AFILIAC/DESAFILIA BG        BQ2C0260BVDBGPO BGMQ26E             BQ26  NS0000CNNNNN    SSTN    C   SNNNSNNN  NN                2016-11-15XP87051 2017-03-1711.32.13XP86453 2016-11-15-08.53.20.092104XP87051 0001-01-010001-01-01
 * FICHERO: PEBT.QGFD.FIX.QGDTFDF.BGMQ26E.D1170920
 * BGMQ26E �PAGO FACIL-LISTA AFILIA/DESAFI�F�08�00061�01�00001�NUMCLIE�CODIGO DE CLIENTE   �A�008�0�O�        �
 * BGMQ26E �PAGO FACIL-LISTA AFILIA/DESAFI�F�08�00061�02�00009�INDISOL�ESTADO DE AFILIACION�A�001�0�O�        �
 * BGMQ26E �PAGO FACIL-LISTA AFILIA/DESAFI�F�08�00061�03�00010�RUBRO  �SERVICIO PUBLICO    �A�002�0�O�        �
 * BGMQ26E �PAGO FACIL-LISTA AFILIA/DESAFI�F�08�00061�04�00012�CONVENI�CODIGO DE CONVENIO  �A�007�0�O�        �
 * BGMQ26E �PAGO FACIL-LISTA AFILIA/DESAFI�F�08�00061�05�00019�FECHINI�FECHA INICIO BUSQUED�A�010�0�O�        �
 * BGMQ26E �PAGO FACIL-LISTA AFILIA/DESAFI�F�08�00061�06�00029�FECHFIN�FECHA FINAL  BUSQUED�A�010�0�O�        �
 * BGMQ26E �PAGO FACIL-LISTA AFILIA/DESAFI�F�08�00061�07�00039�IDPAGIN�IDENT. DE PAGINACION�A�020�0�O�        �
 * BGMQ26E �PAGO FACIL-LISTA AFILIA/DESAFI�F�08�00061�08�00059�TAMPAG �TAMA#O DE PAGINA    �N�003�0�O�        �
 * FICHERO: PEBT.QGFD.FIX.QGDTFDF.BGMQ26P.D1170920
 * BGMQ26P �PAGO FACIL-LISTA AFILIA/DESAFI�X�02�00023�01�00001�IDPAGIN�IDENT. DE PAGINACION�A�020�0�S�        �
 * BGMQ26P �PAGO FACIL-LISTA AFILIA/DESAFI�X�02�00023�02�00021�TAMPAG �TAMA#O DE PAGINA    �N�003�0�S�        �
 * FICHERO: PEBT.QGFD.FIX.QGDTFDF.BGMQ26S.D1170920
 * BGMQ26S �PAGO FACIL-LISTA AFILIA/DESAFI�X�16�00238�01�00001�IDAFIL �IDENT. DE AFILIACION�A�047�0�S�        �
 * BGMQ26S �PAGO FACIL-LISTA AFILIA/DESAFI�X�16�00238�02�00048�RUBRO  �SERVICIO AFILIAR    �A�002�0�S�        �
 * BGMQ26S �PAGO FACIL-LISTA AFILIA/DESAFI�X�16�00238�03�00050�DESCRUB�DESCRIP. RUBRO/SERV.�A�015�0�S�        �
 * BGMQ26S �PAGO FACIL-LISTA AFILIA/DESAFI�X�16�00238�04�00065�CONVENI�CODIGO DE CONVENIO  �A�007�0�S�        �
 * BGMQ26S �PAGO FACIL-LISTA AFILIA/DESAFI�X�16�00238�05�00072�DESCONV�DESCRIPCION CONVENIO�A�015�0�S�        �
 * BGMQ26S �PAGO FACIL-LISTA AFILIA/DESAFI�X�16�00238�06�00087�DESCRAZ�NOMBRE/RAZON SOCIAL �A�030�0�S�        �
 * BGMQ26S �PAGO FACIL-LISTA AFILIA/DESAFI�X�16�00238�07�00117�CODSUMI�CODIGO DE SUMINISTRO�A�030�0�S�        �
 * BGMQ26S �PAGO FACIL-LISTA AFILIA/DESAFI�X�16�00238�08�00147�FECPAG �FECHA PAGO DE RECIBO�A�010�0�S�        �
 * BGMQ26S �PAGO FACIL-LISTA AFILIA/DESAFI�X�16�00238�09�00157�IDENCON�NUMERO DE CONTRATO  �A�020�0�S�        �
 * BGMQ26S �PAGO FACIL-LISTA AFILIA/DESAFI�X�16�00238�10�00177�MAXPAGA�TOPE MAX. IMP. PAGAR�N�015�2�S�        �
 * BGMQ26S �PAGO FACIL-LISTA AFILIA/DESAFI�X�16�00238�11�00192�DIVPAGA�DIVISA DE IMP. PAGAR�A�003�0�S�        �
 * BGMQ26S �PAGO FACIL-LISTA AFILIA/DESAFI�X�16�00238�12�00195�PRIORID�PRIOR. ORDEN DE PAGO�N�003�0�S�        �
 * BGMQ26S �PAGO FACIL-LISTA AFILIA/DESAFI�X�16�00238�13�00198�INDESTA�IND. ESTADO AFILIAC.�A�001�0�S�        �
 * BGMQ26S �PAGO FACIL-LISTA AFILIA/DESAFI�X�16�00238�14�00199�DESCEST�DESCR. ESTADO AFILIA�A�020�0�S�        �
 * BGMQ26S �PAGO FACIL-LISTA AFILIA/DESAFI�X�16�00238�15�00219�FECFAFI�FECHA AFILIACION    �A�010�0�S�        �
 * BGMQ26S �PAGO FACIL-LISTA AFILIA/DESAFI�X�16�00238�16�00229�FECFDES�FECHA DESAFILIACION �A�010�0�S�        �
 * FICHERO: PEBT.QGFD.FIX.QGDTFDX.BQ26.D1170920
 * BQ26BGMQ26P BGNC26P BQ2C02601S                             XP86453 2016-12-19-13.17.15.778760XP86453 2016-12-19-13.17.15.778790
 * BQ26BGMQ26S BGNC26S BQ2C02601S                             XP86453 2016-12-19-13.17.09.487971XP86453 2016-12-19-13.17.09.487995
</pre></code>
 *
 * @see RespuestaTransaccionBq26
 *
 * @author Arquitectura Spring BBVA
 */
@Transaccion(
	nombre = "BQ26",
	tipo = 1,
	subtipo = 1,
	version = 1,
	configuracion = "default_ps9_mx",
	respuesta = RespuestaTransaccionBq26.class,
	atributos = {@Atributo(nombre = "altamiraExtendido", valor = "true"), @Atributo(nombre = "tipoCopy", valor = "FIJA")}
)
@Multiformato(formatos = {FormatoBGMQ26E.class})
@RooJavaBean
@RooToString
@RooSerializable
public class PeticionTransaccionBq26 implements MensajeMultiparte {

	/**
	 * <p>Cuerpo del mensaje de petici&oacute;n multiparte</p>
	 */
	@Cuerpo
	private CuerpoMultiparte cuerpo = new CuerpoMultiparte();

	/**
	 * <p>Permite obtener el cuerpo del mensaje de petici&oacute;n multiparte</p>
	 */
	@Override
	public CuerpoMultiparte getCuerpo() {
		return cuerpo;
	}

}