package com.bbva.pzic.payments.dao.tx.mapper;


import com.bbva.pzic.payments.business.dto.DTOIntAutomaticBillPayments;
import com.bbva.pzic.payments.business.dto.InputListAutomaticBillPayments;
import com.bbva.pzic.payments.canonic.AutomaticBillPayment;
import com.bbva.pzic.payments.dao.model.bq26.FormatoBGMQ26E;
import com.bbva.pzic.payments.dao.model.bq26.FormatoBGMQ26P;
import com.bbva.pzic.payments.dao.model.bq26.FormatoBGMQ26S;

/**
 * Created on 15/08/2018.
 *mapDtoInToRequestFormat
 * @author Entelgy
 */
public interface ITxListAutomaticBillPaymentsMapper {


    FormatoBGMQ26E mapIn(InputListAutomaticBillPayments dtoIn);

    DTOIntAutomaticBillPayments mapOut(FormatoBGMQ26S formatOutput, DTOIntAutomaticBillPayments dtoOut);

    DTOIntAutomaticBillPayments mapOut2(FormatoBGMQ26P formatOutput, DTOIntAutomaticBillPayments dtoOut);
}