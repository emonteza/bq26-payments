package com.bbva.pzic.payments.dao.tx.mapper.impl;

import com.bbva.pzic.payments.business.dto.DTOIntAutomaticBillPayments;
import com.bbva.pzic.payments.business.dto.InputListAutomaticBillPayments;
import com.bbva.pzic.payments.canonic.AutomaticBillPayment;
import com.bbva.pzic.payments.canonic.AutomaticBillPayments;
import com.bbva.pzic.payments.dao.model.bq26.FormatoBGMQ26E;
import com.bbva.pzic.payments.dao.model.bq26.FormatoBGMQ26P;
import com.bbva.pzic.payments.dao.model.bq26.FormatoBGMQ26S;
import com.bbva.pzic.payments.dao.tx.mapper.ITxListAutomaticBillPaymentsMapper;
import com.bbva.pzic.payments.util.mappers.EnumMapper;
import com.bbva.pzic.payments.util.mappers.Mapper;
import com.bbva.pzic.payments.util.orika.MapperFactory;
import com.bbva.pzic.payments.util.orika.converter.builtin.*;
import com.bbva.pzic.payments.util.orika.impl.ConfigurableMapper;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;

/**
 * Created on 15/08/2018.
 *
 * @author Entelgy
 */
@Mapper
public class TxListAutomaticBillPaymentsMapper extends ConfigurableMapper implements ITxListAutomaticBillPaymentsMapper {

    @Autowired
    private EnumMapper enumMapper;

    @Override
    protected void configure(MapperFactory factory) {
        super.configure(factory);

        factory.getConverterFactory().registerConverter(new LongToIntegerConverter());
        factory.getConverterFactory().registerConverter(new DateToStringConverter("yyyy-MM-dd"));
        factory.getConverterFactory().registerConverter(new FromStringConverter());
        factory.getConverterFactory().registerConverter(new BigDecimalToIntegerConverter());


        factory.classMap(InputListAutomaticBillPayments.class, FormatoBGMQ26E.class)
                .field("statusId","indisol")
                .field("serviceServiceTypeId","rubro")
                .field("serviceId","conveni")
                .field("fromStartDate","fechini")
                .field("toStartDate","fechfin")
                .field("paginationKey","idpagin")
                .field("pageSize","tampag")
                .register();

        factory.classMap(FormatoBGMQ26S.class, AutomaticBillPayment.class)
                .field("idafil","id")
                .field("rubro","service.serviceType.id")
                .field("descrub","service.serviceType.name")
                .field("conveni","service.id")
                .field("desconv","service.name")
                .field("descraz","additionalFields[0].value")
                .field("codsumi","bill.id")
                .field("fecpag","startDate")
                .field("idencon","origin.contractId")
                .field("maxpaga","amountMax.value")
                .field("divpaga","amountMax.currency")
                .field("priorid","additionalFields[1].value")
                .field("indesta","status.id")
                .field("descest","status.name")
                .field("fecfafi","activatedDate")
                .field("fecfdes","deactivatedDate")
                .register();

        factory.classMap(FormatoBGMQ26P.class, DTOIntAutomaticBillPayments.class)
                .field("idpagin","pagination.paginationKey")
                .field("tampag","pagination.pageSize")
                .register();
    }


    @Override
    public FormatoBGMQ26E mapIn(final InputListAutomaticBillPayments dtoIn) {
        return map(dtoIn,FormatoBGMQ26E.class);
    }

    @Override
    public DTOIntAutomaticBillPayments mapOut(final FormatoBGMQ26S formatOutput, final DTOIntAutomaticBillPayments dtoOut) {
        if (dtoOut.getData() == null) {
            dtoOut.setData(new ArrayList<>());
        }
        AutomaticBillPayment automaticBillPayment = map(formatOutput,AutomaticBillPayment.class);
        dtoOut.getData().add(automaticBillPayment);
        return dtoOut;
    }

    @Override
    public DTOIntAutomaticBillPayments mapOut2(final FormatoBGMQ26P formatOutput, final DTOIntAutomaticBillPayments dtoOut) {
        map(formatOutput, dtoOut);
        return dtoOut;
    }
}