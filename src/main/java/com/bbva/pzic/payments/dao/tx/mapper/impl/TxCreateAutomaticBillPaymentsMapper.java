package com.bbva.pzic.payments.dao.tx.mapper.impl;

import com.bbva.pzic.payments.dao.tx.mapper.ITxCreateAutomaticBillPaymentsMapper;
import com.bbva.pzic.payments.util.mappers.Mapper;

/**
 * Created on 15/08/2018.
 *
 * @author Entelgy
 */
@Mapper
public class TxCreateAutomaticBillPaymentsMapper
        implements
        ITxCreateAutomaticBillPaymentsMapper {
}