package com.bbva.pzic.payments.dao.model.bq26;

import java.math.BigDecimal;
import java.util.Date;

import com.bbva.jee.arq.spring.core.auditoria.DatoAuditable;
import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.TipoCampo;
import com.bbva.jee.arq.spring.core.host.Formato;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;


/**
 * Formato de datos <code>BGMQ26S</code> de la transacci&oacute;n <code>BQ26</code>
 *
 * @author Arquitectura Spring BBVA
 */
@Formato(nombre = "BGMQ26S")
@RooJavaBean
@RooToString
@RooSerializable
public class FormatoBGMQ26S {

	/**
	 * <p>Campo <code>IDAFIL</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 1, nombre = "IDAFIL", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 47, longitudMaxima = 47)
	private String idafil;

	/**
	 * <p>Campo <code>RUBRO</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 2, nombre = "RUBRO", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 2, longitudMaxima = 2)
	private String rubro;

	/**
	 * <p>Campo <code>DESCRUB</code>, &iacute;ndice: <code>3</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 3, nombre = "DESCRUB", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 15, longitudMaxima = 15)
	private String descrub;

	/**
	 * <p>Campo <code>CONVENI</code>, &iacute;ndice: <code>4</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 4, nombre = "CONVENI", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 7, longitudMaxima = 7)
	private String conveni;

	/**
	 * <p>Campo <code>DESCONV</code>, &iacute;ndice: <code>5</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 5, nombre = "DESCONV", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 15, longitudMaxima = 15)
	private String desconv;

	/**
	 * <p>Campo <code>DESCRAZ</code>, &iacute;ndice: <code>6</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@DatoAuditable(omitir = true)
	@Campo(indice = 6, nombre = "DESCRAZ", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 30, longitudMaxima = 30)
	private String descraz;

	/**
	 * <p>Campo <code>CODSUMI</code>, &iacute;ndice: <code>7</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 7, nombre = "CODSUMI", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 30, longitudMaxima = 30)
	private String codsumi;

	/**
	 * <p>Campo <code>FECPAG</code>, &iacute;ndice: <code>8</code>, tipo: <code>FECHA</code>
	 */
	@Campo(indice = 8, nombre = "FECPAG", tipo = TipoCampo.FECHA, longitudMinima = 10, longitudMaxima = 10, formato = "yyyy-MM-dd")
	private Date fecpag;

	/**
	 * <p>Campo <code>IDENCON</code>, &iacute;ndice: <code>9</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 9, nombre = "IDENCON", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 20, longitudMaxima = 20)
	private String idencon;

	/**
	 * <p>Campo <code>MAXPAGA</code>, &iacute;ndice: <code>10</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 10, nombre = "MAXPAGA", tipo = TipoCampo.DECIMAL, longitudMinima = 15, longitudMaxima = 15, decimales = 2)
	private BigDecimal maxpaga;

	/**
	 * <p>Campo <code>DIVPAGA</code>, &iacute;ndice: <code>11</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 11, nombre = "DIVPAGA", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 3, longitudMaxima = 3)
	private String divpaga;

	/**
	 * <p>Campo <code>PRIORID</code>, &iacute;ndice: <code>12</code>, tipo: <code>ENTERO</code>
	 */
	@Campo(indice = 12, nombre = "PRIORID", tipo = TipoCampo.ENTERO, longitudMinima = 3, longitudMaxima = 3)
	private Integer priorid;

	/**
	 * <p>Campo <code>INDESTA</code>, &iacute;ndice: <code>13</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 13, nombre = "INDESTA", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
	private String indesta;

	/**
	 * <p>Campo <code>DESCEST</code>, &iacute;ndice: <code>14</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 14, nombre = "DESCEST", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 20, longitudMaxima = 20)
	private String descest;

	/**
	 * <p>Campo <code>FECFAFI</code>, &iacute;ndice: <code>15</code>, tipo: <code>FECHA</code>
	 */
	@Campo(indice = 15, nombre = "FECFAFI", tipo = TipoCampo.FECHA, longitudMinima = 10, longitudMaxima = 10, formato = "yyyy-MM-dd")
	private Date fecfafi;

	/**
	 * <p>Campo <code>FECFDES</code>, &iacute;ndice: <code>16</code>, tipo: <code>FECHA</code>
	 */
	@Campo(indice = 16, nombre = "FECFDES", tipo = TipoCampo.FECHA, longitudMinima = 10, longitudMaxima = 10, formato = "yyyy-MM-dd")
	private Date fecfdes;

}