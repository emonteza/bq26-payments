package com.bbva.pzic.payments.dao.model.bq26.mock;

import com.bbva.pzic.payments.dao.model.bq26.FormatoBGMQ26P;
import com.bbva.pzic.payments.dao.model.bq26.FormatoBGMQ26S;
import com.bbva.pzic.payments.util.mappers.ObjectMapperHelper;
import org.codehaus.jackson.type.TypeReference;

import java.io.IOException;
import java.util.List;

/**
 * Created on 2017-03-13.
 *
 * @author Entelgy
 */
public class FormatsBq26Mock {

    private static final FormatsBq26Mock INSTANCE = new FormatsBq26Mock();
    private ObjectMapperHelper objectMapper = ObjectMapperHelper.getInstance();

    public static FormatsBq26Mock getInstance() {
        return INSTANCE;
    }

    public List<FormatoBGMQ26S> getFormatsBGMQ26S() throws IOException {
        return objectMapper.readValue(Thread.currentThread().getContextClassLoader()
                .getResourceAsStream("com/bbva/pzic/payments/dao/model/bq26/mock/formatoBGMQ26S.json"), new TypeReference<List<FormatoBGMQ26S>>() {
        });
    }

    public List<FormatoBGMQ26S> getFormatsBGMQ26SEmpty() throws IOException {
        return objectMapper.readValue(Thread.currentThread().getContextClassLoader()
                .getResourceAsStream("com/bbva/pzic/payments/dao/model/bq26/mock/formatoBGMQ26S_Empty.json"), new TypeReference<List<FormatoBGMQ26S>>() {
        });
    }

    public FormatoBGMQ26P getFormatBGMQ26P() throws IOException {
        return objectMapper.readValue(Thread.currentThread().getContextClassLoader()
                .getResourceAsStream("com/bbva/pzic/payments/dao/model/bq26/mock/formatoBGMQ26P.json"), FormatoBGMQ26P.class);

    }

    public FormatoBGMQ26P getFormatBGMQ26PEmpty() throws IOException {
        return objectMapper.readValue(Thread.currentThread().getContextClassLoader()
                .getResourceAsStream("com/bbva/pzic/payments/dao/model/bq26/mock/formatoBGMQ26P_Empty.json"), FormatoBGMQ26P.class);
    }
}
