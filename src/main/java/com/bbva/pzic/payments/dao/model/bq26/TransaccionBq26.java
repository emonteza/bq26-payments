package com.bbva.pzic.payments.dao.model.bq26;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;

/**
 * Invocador de la transacci&oacute;n <code>BQ26</code>
 * 
 * @see PeticionTransaccionBq26
 * @see RespuestaTransaccionBq26
 */
@Component("transaccionBq26")
public class TransaccionBq26 implements InvocadorTransaccion<PeticionTransaccionBq26,RespuestaTransaccionBq26> {
	
	@Autowired
	private ServicioTransacciones servicioTransacciones;
	
	@Override
	public RespuestaTransaccionBq26 invocar(PeticionTransaccionBq26 transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionBq26.class, RespuestaTransaccionBq26.class, transaccion);
	}
	
	@Override
	public RespuestaTransaccionBq26 invocarCache(PeticionTransaccionBq26 transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionBq26.class, RespuestaTransaccionBq26.class, transaccion);
	}
	
	@Override
	public void vaciarCache() {}	
}