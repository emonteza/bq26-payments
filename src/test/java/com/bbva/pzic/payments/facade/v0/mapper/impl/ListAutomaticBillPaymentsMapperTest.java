package com.bbva.pzic.payments.facade.v0.mapper.impl;

import com.bbva.pzic.payments.EntityMock;
import com.bbva.pzic.payments.business.dto.InputListAutomaticBillPayments;
import com.bbva.pzic.payments.canonic.AutomaticBillPayments;
import com.bbva.pzic.payments.facade.v0.mapper.IListAutomaticBillPaymentsMapper;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Created on 15/08/2018.
 *
 * @author Entelgy
 */
public class ListAutomaticBillPaymentsMapperTest {

    private IListAutomaticBillPaymentsMapper mapper;

    @Before
    public void setUp() {
        mapper = new ListAutomaticBillPaymentsMapper();
    }

    @Test
    public void mapInFullTest() {
        InputListAutomaticBillPayments result = mapper.mapIn(
                EntityMock.PAGINATION_KEY, EntityMock.PAGE_SIZE,
                EntityMock.STATUS_ID, EntityMock.SERVICE_SERVICE_TYPE_ID,
                EntityMock.SERVICE_ID, EntityMock.FROM_START_DATE,
                EntityMock.TO_START_DATE);
        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getPaginationKey());
        Assert.assertNotNull(result.getPageSize());
        Assert.assertNotNull(result.getStatusId());
        Assert.assertNotNull(result.getServiceServiceTypeId());
        Assert.assertNotNull(result.getServiceId());
        Assert.assertNotNull(result.getFromStartDate());
        Assert.assertNotNull(result.getToStartDate());
        Assert.assertEquals(EntityMock.PAGINATION_KEY,
                result.getPaginationKey());
        Assert.assertEquals(EntityMock.PAGE_SIZE, result.getPageSize());
        Assert.assertEquals(EntityMock.STATUS_ID, result.getStatusId());
        Assert.assertEquals(EntityMock.SERVICE_SERVICE_TYPE_ID,
                result.getServiceServiceTypeId());
        Assert.assertEquals(EntityMock.SERVICE_ID, result.getServiceId());
        Assert.assertEquals(EntityMock.FROM_START_DATE,
                result.getFromStartDate());
        Assert.assertEquals(EntityMock.TO_START_DATE, result.getToStartDate());
    }

    @Test
    public void mapInEmptyTest() {
        InputListAutomaticBillPayments result = mapper.mapIn(null, null, null,
                null, null, null, null);
        Assert.assertNotNull(result);
        Assert.assertNull(result.getPaginationKey());
        Assert.assertNull(result.getPageSize());
        Assert.assertNull(result.getStatusId());
        Assert.assertNull(result.getServiceServiceTypeId());
        Assert.assertNull(result.getServiceId());
        Assert.assertNull(result.getFromStartDate());
        Assert.assertNull(result.getToStartDate());
    }

    @Test
    public void mapOutFullTest() {
        AutomaticBillPayments result = mapper.mapOut(EntityMock.getInstance()
                .getDTOIntAutomaticBillPayments());
        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getData());
    }

    @Test
    public void mapOutEmptyTest() {
        AutomaticBillPayments result = mapper.mapOut(null);
        Assert.assertNull(result);
    }
}