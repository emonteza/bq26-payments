package com.bbva.pzic.payments.facade.v0.mapper.impl;

import com.bbva.pzic.payments.EntityMock;
import com.bbva.pzic.payments.business.dto.DTOIntAutomaticBillPayment;
import com.bbva.pzic.payments.canonic.AutomaticBillPayment;
import com.bbva.pzic.payments.canonic.AutomaticBillPaymentData;
import com.bbva.pzic.payments.facade.v0.mapper.ICreateAutomaticBillPaymentsMapper;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

/**
 * Created on 15/08/2018.
 *
 * @author Entelgy
 */
public class CreateAutomaticBillPaymentsMapperTest {

    private ICreateAutomaticBillPaymentsMapper mapper;

    @Before
    public void setUp() {
        mapper = new CreateAutomaticBillPaymentsMapper();
    }

    @Test
    public void mapInFullTest() throws IOException {
        /*AutomaticBillPayment input = EntityMock.getInstance()
                .getAutomaticBillPayment();
        DTOIntAutomaticBillPayment result = mapper.mapIn(input);
        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getId());
        Assert.assertNotNull(result.getService().getServiceType().getId());
        Assert.assertNotNull(result.getService().getServiceType().getName());
        Assert.assertNotNull(result.getService().getId());
        Assert.assertNotNull(result.getService().getName());
        Assert.assertNotNull(result.getAdditionalFields().get(0).getValue());
        Assert.assertNotNull(result.getBill().getId());
        Assert.assertNotNull(result.getStartDate());
        Assert.assertNotNull(result.getOrigin().getContractId());
        Assert.assertNotNull(result.getAmountMax().getValue());
        Assert.assertNotNull(result.getAmountMax().getCurrency());
        Assert.assertNotNull(result.getStatus().getId());
        Assert.assertNotNull(result.getStatus().getName());
        Assert.assertNotNull(result.getActivatedDate());
        Assert.assertNotNull(result.getDeactivatedDate());

        Assert.assertEquals(input.getService().getId(), result.getService().getId());
        Assert.assertEquals(input.getBill().getId(), result.getBill().getId());
        Assert.assertEquals(input.getStartDate(), result.getStartDate());
        Assert.assertEquals(input.getOrigin().getContractId(), result.getOrigin().getContractId());
        Assert.assertEquals(input.getAmountMax().getValue(), result.getAmountMax().getValue());
        Assert.assertEquals(input.getAmountMax().getCurrency(), result.getAmountMax().getCurrency());
        Assert.assertEquals(input.getAdditionalFields().get(0).getValue(),result.getAdditionalFields().get(0).getValue());*/
    }

    @Test
    public void mapInEmptyTest() {
        /*DTOIntAutomaticBillPayment result = mapper
                .mapIn(new AutomaticBillPayment());
        Assert.assertNotNull(result);
        Assert.assertNull(result.getService());
        Assert.assertNull(result.getBill());
        Assert.assertNull(result.getStartDate());
        Assert.assertNull(result.getOrigin());
        Assert.assertNull(result.getAmountMax());
        Assert.assertNull(result.getAdditionalFields());*/
    }

    @Test
    public void mapOutFullTest() {
        AutomaticBillPaymentData result = mapper
                .mapOut(new AutomaticBillPayment());
        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getData());
    }

    @Test
    public void mapOutEmptyTest() {
        AutomaticBillPaymentData result = mapper.mapOut(null);
        Assert.assertNull(result);
    }
}