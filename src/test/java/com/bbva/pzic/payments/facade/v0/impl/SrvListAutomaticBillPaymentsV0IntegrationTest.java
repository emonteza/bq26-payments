package com.bbva.pzic.payments.facade.v0.impl;

import com.bbva.jee.arq.spring.core.servicing.gce.BusinessServiceException;
import com.bbva.jee.arq.spring.core.servicing.test.BusinessServiceTestContextLoader;
import com.bbva.jee.arq.spring.core.servicing.test.MockInvocationContextTestExecutionListener;
import com.bbva.pzic.payments.canonic.AutomaticBillPayments;
import com.bbva.pzic.payments.util.Errors;
import com.bbva.pzic.utilTest.BusinessServiceExceptionMatcher;
import com.bbva.pzic.utilTest.UriInfoImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import static com.bbva.pzic.payments.DummyMock.*;
import static org.junit.Assert.*;

/**
 * Created on 15/08/2018.
 *
 * @author Entelgy
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(loader = BusinessServiceTestContextLoader.class, locations = {
        "classpath*:/META-INF/spring/applicationContext-*.xml",
        "classpath:/META-INF/spring/business-service-test.xml"})
@TestExecutionListeners(listeners = {
        MockInvocationContextTestExecutionListener.class,
        DependencyInjectionTestExecutionListener.class})
public class SrvListAutomaticBillPaymentsV0IntegrationTest {

    @Rule
    public ExpectedException expectedException = ExpectedException.none();
    @Autowired
    private SrvPaymentsV0 srvPaymentsV0;

    @Before
    public void setUp() {
        srvPaymentsV0.setUriInfo(new UriInfoImpl());
    }

    @Test
    public void listAutomaticBillPaymentsTest() {
        AutomaticBillPayments automaticBillPayments = srvPaymentsV0.listAutomaticBillPayments(PAGINATION_KEY, PAGE_SIZE, STATUS_ID, SERVICE_SERVICE_TYPE_ID,SERVICE_ID,FROM_START_DATE,TO_START_DATE);

        assertNotNull(automaticBillPayments.getData());
        assertFalse(automaticBillPayments.getData().isEmpty());
        assertNotNull(automaticBillPayments.getPagination());
    }

    @Test
    public void listAutomaticBillPaymentsTestWithoutPaginationKey() {
        AutomaticBillPayments automaticBillPayments = srvPaymentsV0.listAutomaticBillPayments(null, PAGE_SIZE, STATUS_ID, SERVICE_SERVICE_TYPE_ID,SERVICE_ID,FROM_START_DATE,TO_START_DATE);

        assertNotNull(automaticBillPayments.getData());
        assertFalse(automaticBillPayments.getData().isEmpty());
        assertNotNull(automaticBillPayments.getPagination());

    }

    @Test
    public void listAutomaticBillPaymentsTestWithoutPageSize() {
        AutomaticBillPayments automaticBillPayments = srvPaymentsV0.listAutomaticBillPayments(PAGINATION_KEY, null, STATUS_ID, SERVICE_SERVICE_TYPE_ID,SERVICE_ID,FROM_START_DATE,TO_START_DATE);

        assertNotNull(automaticBillPayments.getData());
        assertFalse(automaticBillPayments.getData().isEmpty());
        assertNotNull(automaticBillPayments.getPagination());

    }

    @Test
    public void listAutomaticBillPaymentsTestWithoutStatusId() {
        AutomaticBillPayments automaticBillPayments = srvPaymentsV0.listAutomaticBillPayments(PAGINATION_KEY, PAGE_SIZE, null, SERVICE_SERVICE_TYPE_ID,SERVICE_ID,FROM_START_DATE,TO_START_DATE);

        assertNotNull(automaticBillPayments.getData());
        assertFalse(automaticBillPayments.getData().isEmpty());
        assertNotNull(automaticBillPayments.getPagination());

    }

    @Test
    public void listAutomaticBillPaymentsTestWithoutServicesServiceTypeId() {
        AutomaticBillPayments automaticBillPayments = srvPaymentsV0.listAutomaticBillPayments(PAGINATION_KEY, PAGE_SIZE, STATUS_ID, null,SERVICE_ID,FROM_START_DATE,TO_START_DATE);

        assertNotNull(automaticBillPayments.getData());
        assertFalse(automaticBillPayments.getData().isEmpty());
        assertNotNull(automaticBillPayments.getPagination());

    }

    @Test
    public void listAutomaticBillPaymentsTestWithoutServicesId() {
        AutomaticBillPayments automaticBillPayments = srvPaymentsV0.listAutomaticBillPayments(PAGINATION_KEY, PAGE_SIZE, STATUS_ID, SERVICE_SERVICE_TYPE_ID,null,FROM_START_DATE,TO_START_DATE);

        assertNotNull(automaticBillPayments.getData());
        assertFalse(automaticBillPayments.getData().isEmpty());
        assertNotNull(automaticBillPayments.getPagination());

    }

    @Test
    public void listAutomaticBillPaymentsTestWithoutFromStartDate() {
        AutomaticBillPayments automaticBillPayments = srvPaymentsV0.listAutomaticBillPayments(PAGINATION_KEY, PAGE_SIZE, STATUS_ID, SERVICE_SERVICE_TYPE_ID,SERVICE_ID,null,TO_START_DATE);

        assertNotNull(automaticBillPayments.getData());
        assertFalse(automaticBillPayments.getData().isEmpty());
        assertNotNull(automaticBillPayments.getPagination());
    }

    @Test
    public void listAutomaticBillPaymentsTestWithoutToStartDate() {
        AutomaticBillPayments automaticBillPayments = srvPaymentsV0.listAutomaticBillPayments(PAGINATION_KEY, PAGE_SIZE, STATUS_ID, SERVICE_SERVICE_TYPE_ID,SERVICE_ID,FROM_START_DATE,null);

        assertNotNull(automaticBillPayments.getData());
        assertFalse(automaticBillPayments.getData().isEmpty());
        assertNotNull(automaticBillPayments.getPagination());
    }

    @Test
    public void listAutomaticBillPaymentsTestWithoutParameters(){
        AutomaticBillPayments automaticBillPayments = srvPaymentsV0.listAutomaticBillPayments(null, null, null, null,null,null,null);

        assertNotNull(automaticBillPayments.getData());
        assertFalse(automaticBillPayments.getData().isEmpty());
        assertNotNull(automaticBillPayments.getPagination());
    }

    @Test
    public void listAutomaticBillPaymentsTestWithWrongSizePaginationKey() {
        expectedException.expect(BusinessServiceException.class);
        expectedException.expect(BusinessServiceExceptionMatcher.hasErrorCode(Errors.WRONG_PARAMETERS));

        srvPaymentsV0.listAutomaticBillPayments("ABC12345678901234567ABC12345678901234567", PAGE_SIZE, STATUS_ID, SERVICE_SERVICE_TYPE_ID,SERVICE_ID,FROM_START_DATE,TO_START_DATE);

    }

    @Test
    public void listAutomaticBillPaymentsTestWithWrongSizePaginationZize() {
        expectedException.expect(BusinessServiceException.class);
        expectedException.expect(BusinessServiceExceptionMatcher.hasErrorCode(Errors.WRONG_PARAMETERS));

        srvPaymentsV0.listAutomaticBillPayments(PAGINATION_KEY, 123345, STATUS_ID, SERVICE_SERVICE_TYPE_ID,SERVICE_ID,FROM_START_DATE,TO_START_DATE);

    }

    @Test
    public void listAutomaticBillPaymentsTestWithWrongSizeStatusId() {
        expectedException.expect(BusinessServiceException.class);
        expectedException.expect(BusinessServiceExceptionMatcher.hasErrorCode(Errors.WRONG_PARAMETERS));

        srvPaymentsV0.listAutomaticBillPayments(PAGINATION_KEY, PAGE_SIZE, "ABC12345ABC12345", SERVICE_SERVICE_TYPE_ID,SERVICE_ID,FROM_START_DATE,TO_START_DATE);

    }

    @Test
    public void listAutomaticBillPaymentsTestWithWrongSizeServicesServiceTypeId() {
        expectedException.expect(BusinessServiceException.class);
        expectedException.expect(BusinessServiceExceptionMatcher.hasErrorCode(Errors.WRONG_PARAMETERS));

        srvPaymentsV0.listAutomaticBillPayments(PAGINATION_KEY, PAGE_SIZE, STATUS_ID, "0212",SERVICE_ID,FROM_START_DATE,TO_START_DATE);

    }

    @Test
    public void listAutomaticBillPaymentsTestWithWrongSizeServicesId() {
        expectedException.expect(BusinessServiceException.class);
        expectedException.expect(BusinessServiceExceptionMatcher.hasErrorCode(Errors.WRONG_PARAMETERS));

        srvPaymentsV0.listAutomaticBillPayments(PAGINATION_KEY, PAGE_SIZE, STATUS_ID, SERVICE_SERVICE_TYPE_ID, "00000650000065",FROM_START_DATE,TO_START_DATE);

    }

    @Test
    public void listAutomaticBillPaymentsTestWithWrongSizeFromStartDate() {
        expectedException.expect(BusinessServiceException.class);
        expectedException.expect(BusinessServiceExceptionMatcher.hasErrorCode(Errors.WRONG_PARAMETERS));

        srvPaymentsV0.listAutomaticBillPayments(PAGINATION_KEY, PAGE_SIZE, STATUS_ID, SERVICE_SERVICE_TYPE_ID, SERVICE_ID, "2016-10-3011",TO_START_DATE);
    }

    @Test
    public void listAutomaticBillPaymentsTestWithWrongSizeToStartDate() {
        expectedException.expect(BusinessServiceException.class);
        expectedException.expect(BusinessServiceExceptionMatcher.hasErrorCode(Errors.WRONG_PARAMETERS));

        srvPaymentsV0.listAutomaticBillPayments(PAGINATION_KEY, PAGE_SIZE, STATUS_ID, SERVICE_SERVICE_TYPE_ID, SERVICE_ID, FROM_START_DATE,"2016-10-3wq11");
    }
}