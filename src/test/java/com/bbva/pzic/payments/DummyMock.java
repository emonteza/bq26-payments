package com.bbva.pzic.payments;

import org.springframework.stereotype.Component;

@Component
public class DummyMock {

    public final static String PAGINATION_KEY = "ABC12345678901234567";
    public final static Integer PAGE_SIZE = 123;
    public final static String STATUS_ID = "a";
    public final static String SERVICE_SERVICE_TYPE_ID = "02";
    public final static String SERVICE_ID = "0000065";
    public final static String FROM_START_DATE = "2016-10-30";
    public final static String TO_START_DATE = "2016-10-30";

}
