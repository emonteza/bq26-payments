package com.bbva.pzic.payments.dao.tx.mapper.impl;


import com.bbva.pzic.payments.EntityMock;
import com.bbva.pzic.payments.business.dto.DTOIntAutomaticBillPayments;
import com.bbva.pzic.payments.business.dto.InputListAutomaticBillPayments;
import com.bbva.pzic.payments.canonic.AutomaticBillPayment;
import com.bbva.pzic.payments.dao.model.bq26.FormatoBGMQ26E;
import com.bbva.pzic.payments.dao.model.bq26.FormatoBGMQ26P;
import com.bbva.pzic.payments.dao.model.bq26.FormatoBGMQ26S;
import com.bbva.pzic.payments.dao.model.bq26.mock.FormatsBq26Mock;
import com.bbva.pzic.payments.util.orika.converter.builtin.BigDecimalToIntegerConverter;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;
import java.math.BigDecimal;

/**
 * Created on 15/08/2018.
 *
 * @author Entelgy
 */
@RunWith(MockitoJUnitRunner.class)
public class TxListAutomaticBillPaymentsMapperTest {

    @InjectMocks
    private TxListAutomaticBillPaymentsMapper mapper;

    @Before
    public void setUp() {

        mapper = new TxListAutomaticBillPaymentsMapper();
    }


    @Test
    public void mapInFullTest() throws IOException{
        InputListAutomaticBillPayments input = EntityMock.getInstance().getInputListAutomaticBillPayments();
        FormatoBGMQ26E result=mapper.mapIn(input);
        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getIndisol());
        Assert.assertNotNull(result.getRubro());
        Assert.assertNotNull(result.getConveni());
        Assert.assertNotNull(result.getFechini());
        Assert.assertNotNull(result.getFechfin());
        Assert.assertNotNull(result.getIdpagin());
        Assert.assertNotNull(result.getTampag());
    }

    @Test
    public void mapInEmptyTest() {
        FormatoBGMQ26E result = mapper.mapIn(new InputListAutomaticBillPayments());
        Assert.assertNotNull(result);
        Assert.assertNull(result.getIndisol());
        Assert.assertNull(result.getRubro());
        Assert.assertNull(result.getConveni());
        Assert.assertNull(result.getFechini());
        Assert.assertNull(result.getFechfin());
        Assert.assertNull(result.getIdpagin());
        Assert.assertNull(result.getTampag());
    }

    @Test
    public void mapOutFullTest() throws IOException {
        DTOIntAutomaticBillPayments result = new DTOIntAutomaticBillPayments();
        FormatoBGMQ26S format= FormatsBq26Mock.getInstance().getFormatsBGMQ26S().get(0);
        result = mapper.mapOut(format,result);
        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getData());
        Assert.assertEquals(1, result.getData().size());

        AutomaticBillPayment automaticBillPayment = result.getData().get(0);
        Assert.assertNotNull(automaticBillPayment);
        Assert.assertNotNull(automaticBillPayment.getId());
        Assert.assertNotNull(automaticBillPayment.getService().getServiceType().getId());
        Assert.assertNotNull(automaticBillPayment.getService().getServiceType().getName());
        Assert.assertNotNull(automaticBillPayment.getService().getId());
        Assert.assertNotNull(automaticBillPayment.getService().getName());
        Assert.assertNotNull(automaticBillPayment.getAdditionalFields().get(0).getValue());
        Assert.assertNotNull(automaticBillPayment.getBill().getId());
        Assert.assertNotNull(automaticBillPayment.getStartDate());
        Assert.assertNotNull(automaticBillPayment.getOrigin().getContractId());
        Assert.assertNotNull(automaticBillPayment.getAmountMax().getValue());
        Assert.assertNotNull(automaticBillPayment.getAmountMax().getCurrency());
        Assert.assertNotNull(automaticBillPayment.getAdditionalFields().get(1).getValue());
        Assert.assertNotNull(automaticBillPayment.getStatus().getId());
        Assert.assertNotNull(automaticBillPayment.getStatus().getName());
        Assert.assertNotNull(automaticBillPayment.getActivatedDate());
        Assert.assertNotNull(automaticBillPayment.getDeactivatedDate());

        format= FormatsBq26Mock.getInstance().getFormatsBGMQ26S().get(1);
        result = mapper.mapOut(format,result);

        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getData());
        Assert.assertEquals(2, result.getData().size());

        automaticBillPayment = result.getData().get(1);
        Assert.assertNotNull(automaticBillPayment);
        Assert.assertNotNull(automaticBillPayment.getId());
        Assert.assertNotNull(automaticBillPayment.getService().getServiceType().getId());
        Assert.assertNotNull(automaticBillPayment.getService().getServiceType().getName());
        Assert.assertNotNull(automaticBillPayment.getService().getId());
        Assert.assertNotNull(automaticBillPayment.getService().getName());
        Assert.assertNotNull(automaticBillPayment.getAdditionalFields().get(0).getValue());
        Assert.assertNotNull(automaticBillPayment.getBill().getId());
        Assert.assertNotNull(automaticBillPayment.getStartDate());
        Assert.assertNotNull(automaticBillPayment.getOrigin().getContractId());
        Assert.assertNotNull(automaticBillPayment.getAmountMax().getValue());
        Assert.assertNotNull(automaticBillPayment.getAmountMax().getCurrency());
        Assert.assertNotNull(automaticBillPayment.getAdditionalFields().get(1).getValue());
        Assert.assertNotNull(automaticBillPayment.getStatus().getId());
        Assert.assertNotNull(automaticBillPayment.getStatus().getName());
        Assert.assertNotNull(automaticBillPayment.getActivatedDate());
        Assert.assertNotNull(automaticBillPayment.getDeactivatedDate());
    }

    @Test
    public void mapOutEmptyTest() throws IOException {
        DTOIntAutomaticBillPayments result = new DTOIntAutomaticBillPayments();
        FormatoBGMQ26S format= FormatsBq26Mock.getInstance().getFormatsBGMQ26SEmpty().get(0);
        result = mapper.mapOut(format, result);
        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getData());
        Assert.assertEquals(1, result.getData().size());
        Assert.assertNotNull(result.getData());

        Assert.assertNull(result.getData().get(0).getId());
        Assert.assertNull(result.getData().get(0).getService());
        Assert.assertNull(result.getData().get(0).getBill());
        Assert.assertNull(result.getData().get(0).getStartDate());
        Assert.assertNull(result.getData().get(0).getOrigin());
        Assert.assertNotNull(result.getData().get(0).getAmountMax().getValue());
        Assert.assertNull(result.getData().get(0).getAmountMax().getCurrency());
        Assert.assertNotNull(result.getData().get(0).getAdditionalFields());
        Assert.assertNull(result.getData().get(0).getStatus());
        Assert.assertNull(result.getData().get(0).getActivatedDate());
        Assert.assertNull(result.getData().get(0).getDeactivatedDate());

    }

    @Test
    public void mapOut2FullTest() throws IOException {
        FormatoBGMQ26P format= FormatsBq26Mock.getInstance().getFormatBGMQ26P();
        DTOIntAutomaticBillPayments result = new DTOIntAutomaticBillPayments();
        result = mapper.mapOut2(format, result);
        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getPagination());
    }

    @Test
    public void mapOut2EmptyTest() throws IOException {
        DTOIntAutomaticBillPayments result = new DTOIntAutomaticBillPayments();
        FormatoBGMQ26P format = FormatsBq26Mock.getInstance().getFormatBGMQ26PEmpty();
        result = mapper.mapOut2(format, result);
        Assert.assertNotNull(result);
        Assert.assertNull(result.getPagination().getPaginationKey());
        Assert.assertNotNull(result.getPagination().getPageSize());
    }
}