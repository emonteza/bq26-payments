package com.bbva.pzic.payments;

import com.bbva.pzic.payments.business.dto.DTOIntAutomaticBillPayments;
import com.bbva.pzic.payments.business.dto.InputListAutomaticBillPayments;
import com.bbva.pzic.payments.canonic.AutomaticBillPayment;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

/**
 * @author Entelgy
 */
public final class EntityMock {

    public final static String PAGINATION_KEY = "ABC12345678901234567";
    public final static Integer PAGE_SIZE = 123;
    public final static String STATUS_ID = "ABC12345";
    public final static String SERVICE_SERVICE_TYPE_ID = "02";
    public final static String SERVICE_ID = "0000065";
    public final static String FROM_START_DATE = "2016-10-30";
    public final static String TO_START_DATE = "2016-10-30";


    private static final EntityMock INSTANCE = new EntityMock();
    private ObjectMapper objectMapper;

    private EntityMock() {
        objectMapper = new ObjectMapper();
        objectMapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd"));
    }

    public static EntityMock getInstance() {
        return INSTANCE;
    }

    public DTOIntAutomaticBillPayments getDTOIntAutomaticBillPayments() {
        DTOIntAutomaticBillPayments dtoIntAutomaticBillPayments = new DTOIntAutomaticBillPayments();
        dtoIntAutomaticBillPayments
                .setData(new ArrayList<AutomaticBillPayment>());
        return dtoIntAutomaticBillPayments;
    }

    public AutomaticBillPayment getAutomaticBillPayment() throws IOException {
        return objectMapper.readValue(Thread.currentThread().getContextClassLoader().getResourceAsStream(
                "mock/automaticBillPayment.json"),
                AutomaticBillPayment.class);
    }

    // TODO Only allowed mock of entities and DTOs

    public InputListAutomaticBillPayments getInputListAutomaticBillPayments() throws IOException {
        return objectMapper.readValue(Thread.currentThread().getContextClassLoader().getResourceAsStream(
                "mock/automaticBillPayment.json"),
                InputListAutomaticBillPayments.class);
    }


}