package com.bbva.pzic.payments.util.mappers;

import com.bbva.jee.arq.spring.core.servicing.gce.BusinessServiceException;
import com.bbva.jee.arq.spring.core.servicing.test.BusinessServiceTestContextLoader;
import com.bbva.jee.arq.spring.core.servicing.test.MockInvocationContextTestExecutionListener;
import com.bbva.pzic.payments.util.Errors;
import com.bbva.pzic.utilTest.BusinessServiceExceptionMatcher;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

/**
 * @author Entelgy
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(loader = BusinessServiceTestContextLoader.class, locations = {
        "classpath*:/META-INF/spring/applicationContext-*.xml",
        "classpath:/META-INF/spring/business-service-test.xml"})
@TestExecutionListeners(listeners = {
        MockInvocationContextTestExecutionListener.class,
        DependencyInjectionTestExecutionListener.class})
public class EnumMapperTest {

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Autowired
    private EnumMapper enumMapper;

    @Test
    public void testGetBackendValue() {
        String value = enumMapper.getBackendValue("addressType.id", "HOME");
        assertEquals(value, "H");
    }

    @Test
    public void testGetNullBackendValue() {
        String value = enumMapper.getBackendValue("addressType.id", null);
        assertNull(value);
    }

    @Test
    public void testGetUnregisterEnumValue() {
        expectedException.expect(BusinessServiceException.class);
        expectedException.expect(BusinessServiceExceptionMatcher.hasErrorCode(Errors.WRONG_PARAMETERS));

        enumMapper.getBackendValue("addressType.id", "X");
    }

    @Test
    public void testGetEnumValue() {
        String value = enumMapper.getEnumValue("addressType.id", "H");
        assertEquals(value, "HOME");
    }

    @Test
    public void testGetNullEnumValue() {
        String value = enumMapper.getEnumValue("addressType.id", null);
        assertNull(value);
    }

    @Test
    public void testGetUnregisterBackendValue() {
        expectedException.expect(BusinessServiceException.class);
        expectedException.expect(BusinessServiceExceptionMatcher.hasErrorCode(Errors.TECHNICAL_ERROR));

        enumMapper.getEnumValue("addressType.id", "XXX");
    }
}